package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;

public class ControllerTest {
	private Controller controller;
	private Patient patient1, patient2, patient4;
	private Laegemiddel middel1, middel2, middel3, middel4;
	private LocalDate day1, day9, day10;
	private LocalTime kl7, kl930, kl12;

	@Before
	public void setUp() throws Exception {
		// Pre-betingelser
		controller = Controller.getController();

		patient1 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
		patient2 = controller.opretPatient("070985-1153", "Finn Madsen", 83.2);
		patient4 = controller.opretPatient("011064-1522", "Ulla Nielsen", 59.9);

		middel1 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		middel2 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		middel3 = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		middel4 = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		day1 = LocalDate.of(2020, 01, 1);
		day9 = LocalDate.of(2020, 01, 9);
		day10 = LocalDate.of(2020, 01, 10);

		kl7 = LocalTime.of(7, 00);
		kl930 = LocalTime.of(9, 30);
		kl12 = LocalTime.of(15, 00);

	}

	@Test
	public void testOpretPNOrdination() {

		// TC1
		try {
			controller.opretPNOrdination(day1, day10, patient1, middel1, -1);
		} catch (IllegalArgumentException iae) {
			assertEquals("Antal skal være større end 0", iae.getMessage());
		}

		// TC2
		try {
			controller.opretPNOrdination(day1, day10, patient1, middel1, 0);
		} catch (IllegalArgumentException iae) {
			assertEquals("Antal skal være større end 0", iae.getMessage());
		}

		// TC3
		Ordination o = controller.opretPNOrdination(day1, day10, patient1, middel1, 1);
		assertNotNull(o);

		// TC4
		try {
			controller.opretPNOrdination(day10, day9, patient1, middel1, 1);
		} catch (IllegalArgumentException iae) {
			assertEquals("Slutdato skal være efter startdato", iae.getMessage());
		}
	}

	@Test
	public void testOpretDagligFastOrdination() {
		// TC1
		try {
			controller.opretDagligFastOrdination(day10, day9, patient1, middel1, 1, 1, 1, 1);
		} catch (IllegalArgumentException iae) {
			assertEquals("Slutdato skal være efter startdato", iae.getMessage());
		}

		// TC2
		Ordination o = controller.opretDagligFastOrdination(day1, day10, patient1, middel1, 1, 1, 1, 1);
		assertNotNull(o);
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		// TC1
		LocalTime[] klokkeSlet = { kl7, kl930, kl12 };
		double[] antalEnheder = { 2, 1 };

		try {
			controller.opretDagligSkaevOrdination(day1, day10, patient1, middel1, klokkeSlet, antalEnheder);
		} catch (IllegalArgumentException iae) {
			assertEquals("Uoverensstemmelse mellem antallet af tidspunkter og antal doser", iae.getMessage());
		}

	}

	@Test
	public void testOpretDagligSkaevOrdination2() {
		LocalTime[] klokkeSlet = { kl7, kl930 };
		double[] antalEnheder = { 2, 1 };

		// TC1
		try {
			controller.opretDagligSkaevOrdination(day10, day9, patient1, middel1, klokkeSlet, antalEnheder);
		} catch (IllegalArgumentException iae) {
			assertEquals("Slutdato skal være efter startdato", iae.getMessage());
		}

		// TC1
		Ordination o = controller.opretDagligSkaevOrdination(day1, day10, patient1, middel1, klokkeSlet, antalEnheder);
		assertNotNull(o);

	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), patient1, middel1, 123);
		controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient1, middel1, 3);
		controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient1, middel1, 3);
		controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient1, middel1, 3);
		controller.opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), patient4, middel3, 5);
		controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), patient1, middel2, 123);
		controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12), patient2, middel2, 2,
				-1, 1, -1);
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24), patient2, middel3,
				kl, an);

		double vægtStart = 50.0;
		double vægtSlut = 100.0;

		// TC1
		int expectedResult = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, middel1);
		assertEquals(4, expectedResult);

		// TC2
		expectedResult = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, middel2);
		assertEquals(2, expectedResult);

		// TC3
		expectedResult = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, middel3);
		assertEquals(2, expectedResult);

		// TC4
		expectedResult = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, middel4);
		assertEquals(0, expectedResult);
	}

}
