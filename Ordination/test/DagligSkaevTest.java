package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligSkaevTest {

	DagligSkaev dagligSkaev;

	@Before
	public void setUp() throws Exception {
		dagligSkaev = new DagligSkaev(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), new Laegemiddel("middel1", 1, 1, 1, "mg"));
	}
	
	// TC1
	@Test
	public void testOpretDosisTC1() {
		// Act
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(12, 15), 5);
		
		// Assert
		assertNotNull(dosis);
		assertEquals(5, dosis.getAntal(), 0.0001);
		assertEquals(LocalTime.of(12, 15), dosis.getTid());	
		}
	
	// TC2
	@Test
	public void testOpretDosisTC2() {
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(125, 15), 5);
		try {
			double actualResult = dagligSkaev.samletDosis();
			fail();
		}
		catch (DateTimeException e) {
			assertEquals(e.getMessage(), "Fejl! Klokkeslettet er større end 24 timer!");
		}
	}
	
	// TC3
	@Test
	public void testOpretDosisTC3() {
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(-12, 15), 5);
		try {
			double actualResult = dagligSkaev.samletDosis();
			fail();
		}
		catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Fejl! Klokkeslettet kan ikke være mindre end 0 timer og 0 minutter!");
		}
	}
	
	// TC4
	@Test
	public void testOpretDosisTC4() {
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(12, -15), 5);
		try {
			double actualResult = dagligSkaev.samletDosis();
			fail();
		}
		catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Fejl! Klokkeslettet kan ikke være mindre end 0 timer og 0 minutter!");
		}
	}
	
	// TC5
	@Test
	public void testOpretDosisTC5() {
		Dosis dosis = dagligSkaev.opretDosis(LocalTime.of(12, 15), -1);
		try {
			double actualResult = dagligSkaev.samletDosis();
			fail();
		}
		catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Fejl! AntalEnheder kan ikke være minus!");
		}
	}
	
	// TC6
	DagligSkaev dagligSkaevTC6;

	@Before
	public void setUpTC6() throws Exception {
		dagligSkaevTC6 = new DagligSkaev(LocalDate.of(2020, 3, 4), LocalDate.of(2020, 4, 2), new Laegemiddel("middel1", 1, 1, 1, "mg"));
	}
	@Test
	public void testOpretDosisTC6() {
		// Act
		Dosis dosis = dagligSkaevTC6.opretDosis(LocalTime.of(12, 15), 5);
		try {
			double actualResult = dagligSkaevTC6.samletDosis();
			fail();
		}
		catch(IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Fejl! Startdato kan ikke være efter slutdato!");
		}
	}
}
