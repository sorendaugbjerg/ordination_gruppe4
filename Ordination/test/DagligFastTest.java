package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligFastTest {

	DagligFast dagligFast;

	@Before
	public void setUp() throws Exception {
		dagligFast = new DagligFast(LocalDate.of(2020, 3, 2), LocalDate.of(2020, 3, 4), new Laegemiddel("middel1", 1, 1, 1, "mg"));
	}

	//TC1
	@Test
	public void testDosisTC1() {
		// Act
		Dosis dosis = dagligFast.opretDosis(LocalTime.of(7,00), 3);

		// Assert
		assertNotNull(dosis);
		assertEquals(3, dosis.getAntal(), 0.0001);
		assertEquals(LocalTime.of(7, 0), dosis.getTid());
	}

	// TC1
	@Test
	public void testOpretDosisTC1() {
		Dosis dosis1 = dagligFast.opretDosis(LocalTime.of(7, 0), 1);
		Dosis dosis2 = dagligFast.opretDosis(LocalTime.of(12, 0), 1);
		Dosis dosis3 = dagligFast.opretDosis(LocalTime.of(17, 0), 1);
		Dosis dosis4 = dagligFast.opretDosis(LocalTime.of(22, 0), 1);
		double actualResult = dagligFast.samletDosis();
		assertEquals(12, actualResult, 0.0001);
	}
	
	// TC2
	@Test
	public void testOpretDosisTC2() {
		Dosis dosis1 = dagligFast.opretDosis(LocalTime.of(7, 0), 4);
		Dosis dosis2 = dagligFast.opretDosis(LocalTime.of(12, 0), 4);
		Dosis dosis3 = dagligFast.opretDosis(LocalTime.of(17, 0), 4);
		Dosis dosis4 = dagligFast.opretDosis(LocalTime.of(22, 0), 4);
		try {
			double actualResult = dagligFast.samletDosis();
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Fejl! Den daglige dosis må max være 4!");
		}
	}
	
	// TC3
	@Test
	public void testOpretDosisTC3() {
		Dosis dosis1 = dagligFast.opretDosis(LocalTime.of(7, 0), 8);
		Dosis dosis2 = dagligFast.opretDosis(LocalTime.of(12, 0), 0);
		Dosis dosis3 = dagligFast.opretDosis(LocalTime.of(17, 0), 0);
		Dosis dosis4 = dagligFast.opretDosis(LocalTime.of(22, 0), 0);
		try {
			double actualResult = dagligFast.samletDosis();
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Fejl! Den daglige dosis må max være 4!");
		}
	}
	
	// TC4
	@Test
	public void testOpretDosisTC4() {
		Dosis dosis1 = dagligFast.opretDosis(LocalTime.of(7, 0), -1);
		Dosis dosis2 = dagligFast.opretDosis(LocalTime.of(12, 0), 0);
		Dosis dosis3 = dagligFast.opretDosis(LocalTime.of(17, 0), 0);
		Dosis dosis4 = dagligFast.opretDosis(LocalTime.of(22, 0), 1);
		try {
			double actualResult = dagligFast.samletDosis();
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Fejl! Der er forsøgt oprettelse af negative dosiser!");
		}
	}
	
	// TC5
	@Test
	public void testOpretDosisTC5() {
		Dosis dosis1 = dagligFast.opretDosis(LocalTime.of(7, 0), 'a');
		Dosis dosis2 = dagligFast.opretDosis(LocalTime.of(12, 0), 'b');
		Dosis dosis3 = dagligFast.opretDosis(LocalTime.of(17, 0), 'c');
		Dosis dosis4 = dagligFast.opretDosis(LocalTime.of(22, 0), 'd');
		try {
			double actualResult = dagligFast.samletDosis();
			fail();
		}
		catch (RuntimeException e) {
			//Kan ikke kaste den *rigtige* fejl, som burde være:
			//"Fejl! Der er forsøgt oprettelse af dosis med andet end tal!"
			//Men den fanger stadig en fejl...
			assertEquals(e.getMessage(), "Fejl! Den daglige dosis må max være 4!");
		}
	}
}
