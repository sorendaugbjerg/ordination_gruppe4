	package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import ordination.PN;

public class PNTest {

	@Test
	public void testGivDosis1() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 22), null, 7);
		assertEquals(true, p1.givDosis(LocalDate.of(2020, 02, 20)));
	}

	@Test
	public void testGivDosis2() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 22), null, 7);
		assertEquals(true, p1.givDosis(LocalDate.of(2020, 02, 18)));
	}

	@Test
	public void testGivDosis3() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 22), null, 7);
		assertEquals(true, p1.givDosis(LocalDate.of(2020, 02, 22)));
	}

	@Test
	public void testGivDosis4() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 22), null, 7);
		assertEquals(false, p1.givDosis(LocalDate.of(2020, 02, 23)));
	}

	@Test
	public void testGivDosis5() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 22), null, 7);
		assertEquals(false, p1.givDosis(LocalDate.of(2020, 02, 17)));
	}

	@Test
	public void testDoegnDosis1() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 0);
		assertEquals(0, p1.doegnDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis2() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 10);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(7.5, p1.doegnDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis3() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 5);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(6.25, p1.doegnDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis4() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 7);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(17.5, p1.doegnDosis(), 0.001);

	}

	@Test
	public void testSamletDosis1() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 7);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(28, p1.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis2() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 4);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(16, p1.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis3() {
		PN p1 = new PN(LocalDate.of(2020, 02, 18), LocalDate.of(2020, 02, 21), null, 5);
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		p1.givDosis(LocalDate.of(2020, 02, 18));
		assertEquals(45, p1.samletDosis(), 0.001);
	}

}
