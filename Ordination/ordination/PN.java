package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> doser;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel leagemiddel, double antal) {
		super(startDen, slutDen, leagemiddel);
		antalEnheder = antal;
		doser = new ArrayList<>();
	}

	public ArrayList<LocalDate> getDoser() {
		return new ArrayList<>(doser);
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.compareTo(getStartDen()) >= 0 && givesDen.compareTo(getSlutDen()) <= 0) {
			doser.add(givesDen);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public double doegnDosis() {
		if (getAntalGangeGivet() <= 0) {
			return 0;
		} else {
			double døgndosis = samletDosis() / super.antalDage();
			return døgndosis;
		}

	}

	@Override
	public double samletDosis() {
		if (getAntalGangeGivet() <= 0) {
			return 0;
		} else {
			double samlet = 0;
			samlet = getAntalGangeGivet() * getAntalEnheder();
			return samlet;
		}

	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		int antal = getDoser().size();
		return antal;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public Laegemiddel getLaegemiddel() {
		return super.getLaegemiddel();
	}

	@Override
	public String getType() {
		return "PN: pro necesare";
	}

}
