package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private ArrayList<Ordination> ordinationer; // Link til Ordination - DONE!

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
		ordinationer = new ArrayList<>(); // Link til Ordination instatieres her - DONE!
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	// Metode (med specifikation) til at vedligeholde link til Ordination - DONE!
	public void addOrdination(Ordination o) {
		if (!ordinationer.contains(o)) {
			ordinationer.add(o);
		}
	}

	// Metode (med specifikation) til at vedligeholde link til Ordination - DONE!
	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(ordinationer);
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

}
