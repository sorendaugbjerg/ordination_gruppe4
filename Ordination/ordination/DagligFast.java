package ordination;

import java.time.LocalDate;
import java.time.LocalTime;	
	

	//Constructor
	public class DagligFast extends Ordination { 
		public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);

	}
	private static Dosis[] dosiser = new Dosis[4]; //Opret array af dosis som max kan have 4 size.


	public Dosis[] getDoser() {
		Dosis[] tempDosis = new Dosis[4];
		for (int i = 0; i < dosiser.length; i++)
		{
			tempDosis[i] = dosiser[i];
		}
		return tempDosis;
	}
	
	public Dosis opretDosis(LocalTime tid, double antal) { //Controller(Linje 58) skal bruge en opretDosis
		Dosis dosis = new Dosis(tid, antal); //Tid er med 5 timers mellemrum.
		
		if (tid.equals(LocalTime.of(7, 0))) { //Klokken 7 om morgen
			dosiser[0] = dosis; //antal dosis.
		}
		else if (tid.equals(LocalTime.of(12, 0))) { //Klokken 12 om dagen
			dosiser[1] = dosis;
		}
		else if (tid.equals(LocalTime.of(17, 0))) { //Klokken 17 om aftenen
			dosiser[2] = dosis;
		}
		else if (tid.equals(LocalTime.of(22, 0))) { //Klokken 22 om natten.
			dosiser[3] = dosis;
		}
		return dosis;
	}

	@SuppressWarnings("unlikely-arg-type") //Til if-sætning equals A-Za-z+. For at fange bogstaver som fejl.
	@Override
	public double samletDosis() {
		double sum = 0; //Sum her er den samlede dosis.
		for (int i = 0; i <= 3; i++) { //Vi løber listen igennem. Max 4 dosiser i DagligFast.
			if(dosiser[i].getAntal() < 0) { //Fejl catching. Må ikke oprette dosiser med negativ value.
				throw new RuntimeException("Fejl! Der er forsøgt oprettelse af negative dosiser!");
			}
			
			if(dosiser[i] != null) { //Hvis 'i' ikke er en tom værdi, har vi et match.
				sum += dosiser[i].getAntal(); //få antallet af 'i' fra et index i arrayet og læg det i sum.
			}
			
			//Denne if sætning ser ikke ud til at virke. Men den ødelægger vist heller ikke noget.
			if(dosiser[i].equals("[^A-Za-z]+")) {
				throw new RuntimeException("Fejl! Der er forsøgt oprettelse af dosis med andet end tal!");
			}
		}
		if(sum < 0) { //Fejl catching. Hele summen er i minus.
			throw new RuntimeException("Fejl! Der er forsøgt oprettelse af en negativ sum af dosiser!");
		}
		if(sum == 0) { //Fejl catching. Hele summen er nul. Der skal være minium 1 dosis.
			throw new RuntimeException("Fejl! Der er forsøgt oprettelse af tomme dosiser!");
		}
		if(sum / 4 > 1) {
			throw new RuntimeException("Fejl! Den daglige dosis må max være 4!");
		}
		//Sum er kun én dags ordinering. Ganget med de antal dage det tages over får vi det samlede antal dosiser.
		return sum * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		/*
		 * Den samlede dosis divideret over antal dage giver os et gennemsnit.
		 * Gennemsnittet er det tal der skal bruges pr. dag,
		 * hvilket er døgn dosis (doegnDosis)..
		 */
		return this.samletDosis() / super.antalDage(); 
	}

	@Override
	public String getType() {
		//"Typen" er bare DagligFast, så den returner bare en string med den tekst.
		return "Daglig Fast";
	}
}
