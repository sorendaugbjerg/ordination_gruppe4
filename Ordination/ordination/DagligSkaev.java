package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	public Dosis opretDosis(LocalTime klokkeSlet, double antalEnheder) {
		if (antalEnheder == 0) {
			throw new IllegalArgumentException("Fejl! AntalEnheder kan ikke være 0!");
		}
		if(antalEnheder < 0) {
			throw new IllegalArgumentException("Fejl! AntalEnheder kan ikke være minus!");
		}
//		//Hvis klokkeslettet er større end 23:59
//		if(klokkeSlet > 24) {
//			throw new DateTimeException("Fejl! Klokkeslettet kan ikke være større end 24 timer!");
//		}
//		//Hvis klokke slettet er i minus (Hvis klokke 
//		if(klokkeSlet < 0) {
//			throw new DateTimeException("Fejl! Klokkeslettet kan ikke være mindre end 0 timer og 0 minutter!");
//
//		}
		Dosis dosis = new Dosis(klokkeSlet, antalEnheder);
		doser.add(dosis);

		return dosis;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}
}
